package di.uniba.it.progettomobile;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.TextView;

import java.sql.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainActivity extends AppCompatActivity {

    TextView textView;

    //private static final String url = "jdbc:mysql://progetto-mobile-2021.mysql.database.azure.com:3306/sms?useSSL=true&requireSSL=false";
    //?useSSL=true&requireSSL=false
    //private static final String user = "AdminDB@progetto-mobile-2021";
    //private static final String password = "esameSMS2021";
    private static final Database db = new Database();
    ExecutorService executor = Executors.newSingleThreadExecutor();
    Handler handler = new Handler(Looper.getMainLooper());


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.mostraDati);


    }

    public void richiestaQuery(View view)
    {
        backGroundWork();
    }

    public void backGroundWork()
    {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                String query = "";
                try {
                    query = db.dbCreate();
                    //textView.setText(query);
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                    textView.setText(throwables.toString());
                }

                String finalQuery = query;

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        textView.setText(finalQuery);
                    }
                });//end handler Method

            }//end Run Method
        });
    }

    //INIZIO NUOVO CODICE



    //FINE NUOVO CODICE

/* BACKUP
    public void richiestaQuery(View view)
    {
        connessione();
    }

    //AsyncTask viene utilizzato per caricare in anticipo la connessione al Database

    private class connessione extends AsyncTask<String, Void, String>
    {
       @Override
       protected String doInBackground(String...params)
       {
           String query = "";
           try {
               query = db.dbCreate();
               //textView.setText(query);
           } catch (SQLException throwables) {
               throwables.printStackTrace();
               textView.setText(throwables.toString());
           }
        return query;
       }

       @Override
        protected void onPostExecute(String query)
       {
           textView.setText(query);
       }
    }

    public void connessione()
    {
        connessione task = new connessione();
        task.execute();
    }
    * */
}




